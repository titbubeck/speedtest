#!/usr/bin/env python3

import sys
import random

#time stuff
import time
import argparse
import my_functions

start_time = time.perf_counter()

# default variables
default_url_file       = 'sources.list'
default_dl_dest        = '/dev/null'
default_max_runtime    = 6
default_max_downloads  = 4
default_bwmon_interval = 1
default_bwmon_units    = 'B'

default_influxdb_port   = 8086
default_influxdb_dbname = 'bwstats'



# arguments
parser = argparse.ArgumentParser()
parser.add_argument( "--max-runtime",       type=int,  help="set max runtime (seconds)",                               default=default_max_runtime    )
parser.add_argument( "--max-downloads",     type=int,  help="set max downloads number",                                default=default_max_downloads  )
parser.add_argument( "--dl-destination",    type=str,  help="set download destination (directory)",                    default=default_dl_dest        )
parser.add_argument( "--url-file",          type=open, help="file with URLs for Down/Up loads",                        default=default_url_file       )
parser.add_argument( "--bwmon-interval",    type=int,  help="interface statistics update interval",                    default=default_bwmon_interval )
parser.add_argument( "--bwmon-units",       type=str,  help="interface statistics units (M,K,B)",                      default=default_bwmon_units    )
parser.add_argument( "--bwmon-interface",   type=str,  help="interface to monitor bandwidth",        action='append',                                 )
parser.add_argument( "--quiet",                        help="suppress commandline output",           action='count',   default=0                      )
parser.add_argument( "--debug",                        help="raise commandline output",              action='count',   default=0                      )
parser.add_argument( "--influxdb-host",     type=str,  help="send result to specified influxdb server/host",                                          )
parser.add_argument( "--influxdb-dbname",   type=str,  help="DB name for specified influxdb server/host",              default=default_influxdb_dbname)
parser.add_argument( "--influxdb-port",     type=int,  help="port for specified influxdb server",                      default=default_influxdb_port  )
args = parser.parse_args()


#get urls from file
my_urls  = {}
for line in args.url_file:
    url = line.strip()
    if ':' in url:
        my_site = url.split(':')[1].lstrip('/').split('/')[0]
        if my_site not in my_urls: my_urls[my_site] = []
        my_urls[my_site].append(url)

#shuffle dict with urls
my_urls_items = list(my_urls.items())
random.shuffle(my_urls_items)
my_urls = dict(my_urls_items)

#start downloads
running_downloads = 0
dl_objects = []
while running_downloads < args.max_downloads:
    for site, urls in my_urls.items():
        exit_flag = 1 
        if running_downloads < args.max_downloads and len(urls) > 0:
            my_url = urls.pop(0)
            dl_object = my_functions.run_sub(my_functions.get_url, (my_url,))
            dl_objects.append(dl_object)
            if args.quiet == 0: print('downloading: {}'.format(my_url))
            running_downloads += 1
            exit_flag = 0
    if exit_flag == 1:
        running_downloads = args.max_downloads

# skip the first second for measurments
time.sleep(1)

#get interface infos
my_ifaces_raw = my_functions.GetNetworkInterfaces()
my_stats      = my_functions.init_interface_stats(my_ifaces_raw[0], my_ifaces_raw[1])

#check runtime, show stats, kill downloads
my_runtime = 0
while True:
    time.sleep(args.bwmon_interval)
    if args.bwmon_interface is not None:
        my_functions.update_bw_raw(my_stats)
        my_functions.calc_bw(my_stats, args.bwmon_interface)
        if args.quiet == 0: my_functions.print_bw(my_stats, args.bwmon_interface, args.bwmon_units)

    if (time.perf_counter() - start_time) >= args.max_runtime:
        for my_object in dl_objects:
            my_functions.stop_sub(my_object, args.debug)
        break

#bw summary stats
my_functions.calc_bw_sum(my_stats, args.bwmon_interface, args.bwmon_units)
if args.quiet == 0: my_functions.print_bw_sum_new(my_stats, args.bwmon_interface, args.bwmon_units)
if args.influxdb_host is not None:
    influx_data = my_functions.data_prepare_influx(my_stats, args.bwmon_interface, [args.influxdb_host,args.influxdb_port,args.influxdb_dbname])
    my_functions.post_data(influx_data[0],influx_data[1], args.debug)

end_time = time.perf_counter()
if args.quiet == 0: print('Total runtime: {}'.format(end_time - start_time))


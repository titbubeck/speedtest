# url dl stuff
import urllib.request
#processing stuff
from multiprocessing import Process
#time stuff
import time

#further data processing
#import json
import requests
#import socket


# run subprocess
def run_sub(my_target, my_args):
    p = Process(target=my_target, args=my_args)
    p.start()
    return p

# stop subprocess
def stop_sub(my_object, debug=0):
    if debug > 0: print('closing: {}'.format(my_object))
    my_object.terminate()
    if debug > 0: print('closed: {}'.format(my_object))
    return True

# update_runtime
def update_runtime(my_start_time):
    my_runtime = time.perf_counter() - my_start_time
    return my_runtime

#retrieve URL
def get_url(my_url, my_dest='/dev/null'):
    urllib.request.urlretrieve(my_url, my_dest)
    return True

# prepare data for influxdb post
def data_prepare_influx(my_data, my_ifaces, my_influx_cfg):
    influx_data = []
    my_url = 'http://{}:{}/write?db={}'.format(my_influx_cfg[0], my_influx_cfg[1], my_influx_cfg[2])
    for my_iface in my_ifaces:
        if my_iface in my_data:
            my_max = my_data[my_iface]['peaks']
            my_avg = my_data[my_iface]['average']
            secs_passed = round(my_data[my_iface]['total']['seconds_passed'])
            influx_data.append('speedtest,location=w20,uplink=uplink1,ifname={1},runtime={0} avgdown={2},maxdown={3},avgdownbit={4},maxdownbit={5}'.format(secs_passed,my_iface,my_avg['B_rx'],my_max['max_Bps_rx'], my_avg['B_rx']*8, my_max['max_Bps_rx']*8 ) )
    influx_string = '\n'.join(influx_data)
    return [my_url, influx_string]

def post_data(my_url, my_data, debug=0):
    if debug > 0: print('POST url: {}\POST data: {}'.format(my_url, my_data))
    my_headers={'content-type': 'application/x-www-form-urlencoded'}
    #response = urllib.request.urlopen(req)
    r  = requests.post(my_url, data=my_data, headers=my_headers)
    if debug > 0: print('Status-Code Response: {}'.format(r.status_code))

#def write_socket(my_server_cfg, my_data):
#    print(type(my_data))
#    print(my_data)
#    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
#        s.connect(my_server_cfg)
#        s.sendall(my_data)
#        data = s.recv(1024)
#    print('Received', repr(data))
#    return


#convert bw units
def convert_units(my_data,my_unit):
    if 'M' in my_unit:
        my_data = my_data / 1024 / 1024
    elif 'K' in my_unit:
        my_data = my_data / 1024
    if 'b' in my_unit:
        my_data = my_data * 8
    return my_data



# update bw, do some calculations and store results, output should be more or less ready for printing or json converting or whatever
def calc_bw(my_data, my_ifaces, ):
    for my_iface in my_ifaces:
        if my_iface in my_data:
            my_Xps = my_data[my_iface]['current_Xps']
            my_bytes_passed   = my_data[my_iface]['bytes_passed']
            my_packets_passed = my_data[my_iface]['packets_passed']
            my_seconds_passed = my_data[my_iface]['seconds_passed']
            my_Xps['B_rx'] = round(my_bytes_passed['rx'] / my_seconds_passed, 2)
            my_Xps['B_tx'] = round(my_bytes_passed['tx'] / my_seconds_passed, 2)
            my_Xps['p_rx'] = round(my_packets_passed['rx'] / my_seconds_passed, 2)
            my_Xps['p_tx'] = round(my_packets_passed['tx'] / my_seconds_passed, 2)
            if my_Xps['B_rx'] > my_data[my_iface]['peaks']['max_Bps_rx']: my_data[my_iface]['peaks']['max_Bps_rx'] = my_Xps['B_rx']
            if my_Xps['p_rx'] > my_data[my_iface]['peaks']['max_pps_rx']: my_data[my_iface]['peaks']['max_pps_rx'] = my_Xps['p_rx']
    return True

#print bw, interval needed for time unit calc
def print_bw(my_data, my_ifaces, my_unit='B'):
    for my_iface in my_ifaces:
        if my_iface in my_data:
            my_Xps = my_data[my_iface]['current_Xps']

            ingress_col_Xps = 'Ingress: {:.2f} {}ps'.format(convert_units(my_Xps['B_rx'], my_unit), my_unit)
            ingress_col_pps = ' (pps {:.0f})'.format(my_Xps['p_rx'])
            ingress_col     = '{0:<}{1:>10}'.format(ingress_col_Xps,ingress_col_pps)

            egress_col_Xps  = 'Egress: {:.2f} {}ps'.format(convert_units(my_Xps['B_tx'], my_unit), my_unit)
            egress_col_pps  = ' (pps {:.0f})'.format(my_Xps['p_tx'])
            egress_col      = '{0:<}{1:<}'.format(egress_col_Xps,egress_col_pps)

            output_line = 'Interface: {0:<20}\t{1:<40}{2:<40}'.format(my_iface, ingress_col, egress_col)
            print(output_line)
    return

#calculate bw summary
def calc_bw_sum(my_data, my_ifaces, my_unit='B'):
    for my_iface in my_ifaces:
        if my_iface in my_data:
            my_rx_bytes_passed        = my_data[my_iface]['last_stats']['rx']['bytes']   - my_data[my_iface]['first_stats']['rx']['bytes']
            my_tx_bytes_passed        = my_data[my_iface]['last_stats']['tx']['bytes']   - my_data[my_iface]['first_stats']['tx']['bytes']
            my_rx_packets_passed      = my_data[my_iface]['last_stats']['rx']['packets'] - my_data[my_iface]['first_stats']['rx']['packets']
            my_tx_packets_passed      = my_data[my_iface]['last_stats']['tx']['packets'] - my_data[my_iface]['first_stats']['tx']['packets']
            my_total_seconds_passed   = my_data[my_iface]['last_timestamp'] - my_data[my_iface]['first_timestamp']
            my_average_B_rx           = round(my_rx_bytes_passed / my_total_seconds_passed, 2) 
            my_average_B_tx           = round(my_tx_bytes_passed / my_total_seconds_passed, 2) 
            my_average_p_rx           = round(my_rx_packets_passed / my_total_seconds_passed, 2) 
            my_average_p_tx           = round(my_tx_packets_passed / my_total_seconds_passed, 2) 

            my_data[my_iface]['average'] = {'B_rx':my_average_B_rx, 'B_tx':my_average_B_tx, 'p_rx':my_average_p_rx, 'p_tx':my_average_p_tx}
            my_data[my_iface]['total']   = {'B_rx':my_rx_bytes_passed, 'B_tx':my_tx_bytes_passed, 'p_rx':my_rx_packets_passed, 'p_tx':my_tx_packets_passed, 'seconds_passed':my_total_seconds_passed}
    return True


#print bandwidth summary
def print_bw_sum_new(my_data, my_ifaces, my_unit='B'):
    for my_iface in my_ifaces:
        if my_iface in my_data:
            my_avg_items = my_data[my_iface]['average']
            line_sum         = 'Interface: {0:<20}\tAverage\t\tIngress: {1:.2f} {5}/s\t({2:.0f}pps)\t\tEgress: {3:.2f}{5}/s\t({4:.0f} pps) '.format(my_iface, convert_units(my_avg_items['B_rx'],my_unit), my_avg_items['p_rx'], convert_units(my_avg_items['B_tx'],my_unit), my_avg_items['p_tx'], my_unit)
            my_tot_items = my_data[my_iface]['total']
            line_sum_raw     = 'Interface: {0:<20}\tTotal\t\tIngress: {1:.2f} {5}\t({2} packets)\tEgress: {3:.2f}{5}\t({4} packets) '.format(my_iface, convert_units(my_tot_items['B_rx'],my_unit), my_tot_items['p_rx'], convert_units(my_tot_items['B_tx'],my_unit), my_tot_items['B_tx'], my_unit)
            print(line_sum)
            print(line_sum_raw)


def update_bw_raw(my_data):
    my_new_data_all  = GetNetworkInterfaces()
    my_new_data      = my_new_data_all[0]
    my_new_timestamp = my_new_data_all[1]
    for my_new_iface, my_new_stats in my_new_data.items():
        if my_new_iface in my_data:
            my_last_stats = my_data[my_new_iface]['last_stats']
            for my_type_direction in ['rx', 'tx']:
                for my_type_data in ['bytes','packets']:
                    fix_dict_key = '{}_passed'.format(my_type_data)
                    my_numbers      = my_new_stats[my_type_direction][my_type_data]
                    my_numbers_last = my_last_stats[my_type_direction][my_type_data]
                    my_data[my_new_iface][fix_dict_key][my_type_direction] = my_numbers - my_numbers_last
            my_data[my_new_iface]['seconds_passed'] = my_new_timestamp - my_data[my_new_iface]['last_timestamp']
            my_data[my_new_iface]['last_timestamp'] = my_new_timestamp
            my_data[my_new_iface]['last_stats']     = my_new_data[my_new_iface]

    return True

# convert networkifaces output from function below
def init_interface_stats(my_data, my_timestamp):
    my_iface_stats = {}
    for my_iface, my_stats in my_data.items():
        my_iface_stats[my_iface] = { 'first_stats': my_stats, 'last_stats': my_stats, 'first_timestamp': my_timestamp, 'last_timestamp': my_timestamp, 'seconds_passed':0, 'bytes_passed': {'rx':0,'tx':0}, 'packets_passed': {'rx':0,'tx':0}, 'peaks':{'max_Bps_rx':0, 'max_pps_rx':0}, 'current_Xps':{} } 
    return my_iface_stats


def GetNetworkInterfaces():
    ifaces = {}
    my_timestamp = time.perf_counter()
    f = open("/proc/net/dev")
    data = f.read()
    f.close()
    data = data.split("\n")[2:]
    for i in data:
        if len(i.strip()) > 0:
            x = i.split()
            # Interface |                        Receive                          |                         Transmit
            #   iface   | bytes packets errs drop fifo frame compressed multicast | bytes packets errs drop fifo frame compressed multicast
            ifaces[ x[0][:len( x[0])-1] ] = {
                "rx"        :   {
                    "bytes"         :   int(x[1]),
                    "packets"       :   int(x[2]),
                    "errs"          :   int(x[3]),
                    "drop"          :   int(x[4]),
                    "fifo"          :   int(x[5]),
                    "frame"         :   int(x[6]),
                    "compressed"    :   int(x[7]),
                    "multicast"     :   int(x[8])
                },
                "tx"        :   {
                    "bytes"         :   int(x[9]),
                    "packets"       :   int(x[10]),
                    "errs"          :   int(x[11]),
                    "drop"          :   int(x[12]),
                    "fifo"          :   int(x[13]),
                    "frame"         :   int(x[14]),
                    "compressed"    :   int(x[15]),
                    "multicast"     :   int(x[16])
                }
            }
    return [ifaces, my_timestamp]


**check max bandwidth **

try to mmeasure maximum bandwidth and output stats on commandline or write to grafana

some parts are copy&paste from internet, the others trail&error as im just a sysadmin and no developer

this script is measuring the complete bandwidth going over a link (no matter which connection, host, application or whatsoever),
    
## what is working:
a given file with URLs get parsed,

downloads get started parallel and the downlink bandwith of a given link (or all) is measured and calculated to Unit/sec,

the script will kill the downloads after a maximum number of seconds (configurable),

then outputs and/or write to grafana:
* the Average measured Units/s
* the maximum measured Units/sec
    
## todos:
measure *uplink* bandwidth -> upload data to given host

better calculate to bit/s in the function reading the iface-stats instead of in the end/after all calculations

catch ctrl+c user input (print stats instead of the python output)